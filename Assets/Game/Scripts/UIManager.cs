﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public Text m_loggingInText;
    public Text m_totalPokemonCaughtText;
    public GameObject m_loginPanel;
    public InputField m_usernameInputField;
    public Text m_welcomeText;
    public Text m_setUsernameButtonText;
    public string m_noNameWelcomeString = "Welcome! (Please set your username)";
    public string m_WelcomeString = "Welcome";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetWelcomeString(string p_displayName)
    {
        if(string.IsNullOrEmpty(p_displayName))
        {
            m_welcomeText.text = m_noNameWelcomeString;
            m_setUsernameButtonText.text = "Set username";
        }
        else
        {
            m_welcomeText.text = m_WelcomeString+" "+ p_displayName + "!";
            m_setUsernameButtonText.text = "Update username";
        }
    }

    public string GetUsername()
    {
        return m_usernameInputField.text;
    }

    public void SetUsernametoEmpty()
    {
        m_usernameInputField.text = string.Empty;
    }
}

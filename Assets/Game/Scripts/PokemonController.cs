﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PokemonController : MonoBehaviour
{

    [System.Serializable]
    public class SpriteType
    {
        public string back_default;
        public string back_female;
        public string back_shiny;
        public string back_shiny_female;
        public string front_default;
        public string front_female;
        public string front_shiny;
        public string front_shiny_female;
    }

    [System.Serializable]
    public class Pokemon
    {
        public string name;
        public SpriteType sprites;
    }


    public InputField m_pokedexIndexInputField;
    public Text m_pokemonErrorText;
    public Text m_pokemonNameText;
    public string m_errorIfPokemonNotFoundString = "No Pokemon Found!";
    public string m_errorIfPokemonImageNotFoundString = "Pokemon Found but there's no image available!";
    public Image m_pokemonImage;
    private Color m_emptyColor ;
    void Start()
    {
        m_emptyColor = new Color(m_pokemonImage.color.r, m_pokemonImage.color.g, m_pokemonImage.color.b, 0);
        m_pokemonImage.color = m_emptyColor;
    }

    public void GetPokemon()
    {
        if (m_pokemonErrorText.enabled)
        {
            m_pokemonErrorText.enabled = false;
        }
        string a_URL = "https://pokeapi.co/api/v2/pokemon/{0}";
        string a_pokedexIndex = "";
        if (m_pokedexIndexInputField != null)
        {
            a_pokedexIndex = m_pokedexIndexInputField.text;
        }

        if (!string.IsNullOrEmpty(a_pokedexIndex))
        {
            Pokemon a_pokemonObject = GetPokemonObject(a_URL, a_pokedexIndex);

            if (a_pokemonObject != null)
            {
                if (a_pokemonObject.sprites != null && (!string.IsNullOrEmpty(a_pokemonObject.name)))
                {
                    m_pokemonNameText.text = a_pokemonObject.name.ToUpper();
                    if (!string.IsNullOrEmpty(a_pokemonObject.sprites.front_default))
                    {

                        StartCoroutine(SetSprite(a_pokemonObject.sprites.front_default));
                    }
                    else
                    {
                        m_pokemonErrorText.text = m_errorIfPokemonImageNotFoundString;
                        m_pokemonErrorText.enabled = true;
                        m_pokemonImage.sprite = null;
                        m_pokemonImage.color = m_emptyColor;
                    }
                }
                else
                {
                    m_pokemonImage.sprite = null;
                    m_pokemonImage.color = m_emptyColor;
                }
            }
            else
            {
                m_pokemonNameText.text = string.Empty;
                m_pokemonImage.sprite = null;
                m_pokemonImage.color = m_emptyColor;
                m_pokemonErrorText.text = m_errorIfPokemonNotFoundString;
                m_pokemonErrorText.enabled = true;

            }
        }
        else
        {
            m_pokemonNameText.text = string.Empty;
            m_pokemonImage.sprite = null;
            m_pokemonImage.color = m_emptyColor;
        }
    }

    IEnumerator SetSprite(string p_spriteURL)
    {
        UnityWebRequest a_webRequest = UnityWebRequest.Get(p_spriteURL);

        yield return a_webRequest.SendWebRequest();

        if (a_webRequest.isNetworkError || a_webRequest.isHttpError)
        {
            Debug.Log(a_webRequest.error);
        }
        else
        {
            byte[] a_texData = a_webRequest.downloadHandler.data;
            Texture2D a_texture2D = new Texture2D(1, 1);
            a_texture2D.LoadImage(a_texData);
            Sprite a_pokemonSprite = Sprite.Create(a_texture2D, new Rect(0, 0, a_texture2D.width, a_texture2D.height), new Vector2(0.5f, 0.5f), 100.0f);
            m_pokemonImage.sprite = a_pokemonSprite;
            m_pokemonImage.color = new Color(m_pokemonImage.color.r, m_pokemonImage.color.g, m_pokemonImage.color.b, 1);
        }
    }


    private Pokemon GetPokemonObject(string p_apiURL, string p_pokedexIndex)
    {
        StreamReader a_reader = null;
        HttpWebRequest a_request = null;
        HttpWebResponse a_response = null;
        try
        {
            p_apiURL = string.Format(p_apiURL, p_pokedexIndex);

            a_request = WebRequest.CreateHttp(p_apiURL);
            a_response = a_request.GetResponse() as HttpWebResponse;
            a_reader = new StreamReader(a_response.GetResponseStream());

            string a_jsonResponse = a_reader.ReadToEnd();
            return JsonUtility.FromJson<Pokemon>(a_jsonResponse);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
            return null;
        }
        finally
        {
            if (a_reader != null)
            {
                a_reader.Close();
                a_reader.Dispose();
            }
        }
    }
}

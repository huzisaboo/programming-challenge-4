﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationServiceController : Singleton<LocationServiceController>
{
    public UploadCaughtPokemonData m_uploadClassReference;
    public enum LocationServicesState
    {
        Waiting,
        Searching,
        Ready,
        Failed
    }

    public LocationServicesState m_state = LocationServicesState.Waiting;

    private string m_latitude;
    private string m_longitude;


    public void InitializeLocation()
    {

        m_state = LocationServicesState.Searching;
        StartCoroutine(LocationServiceUpdate(m_uploadClassReference.UploadData));
    }


    IEnumerator LocationServiceUpdate(Action<string,string> onComplete)
    {
        Input.location.Start();
        int a_maxWaitTime = 5;

        while (Input.location.status == LocationServiceStatus.Initializing && a_maxWaitTime > 0)
        {
            yield return new WaitForSeconds(1);
            a_maxWaitTime--;
        }

        if (a_maxWaitTime <= 0)
        {
            m_latitude = "Unavailable, Service Time Out";
            m_longitude = "Unavailable, Service Time Out";
            yield break;
        }

      

        m_latitude = Input.location.lastData.latitude.ToString();
        m_longitude = Input.location.lastData.longitude.ToString();

        Input.location.Stop();

        m_state = LocationServicesState.Ready;

        onComplete(m_latitude,m_longitude);
    }

}

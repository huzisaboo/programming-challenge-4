﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UploadCaughtPokemonData : MonoBehaviour
{
    public PokemonController m_pokemonController;
    public Leaderboard m_leaderboard;
    private string m_pokemonName;
    private int m_pokemonCount;
    

    public void OnCatchButtonClicked()
    {
        m_pokemonCount = m_leaderboard.GetPokemonCount();
        m_pokemonName = m_pokemonController.m_pokemonNameText.text;

        LocationServiceController.Instance.InitializeLocation();
    }

    public void UploadData(string p_latitude,string p_longitude)    //Uploads all relevant data to Playfab
    {
        if (!string.IsNullOrEmpty(m_pokemonName))

            PlayFabClientAPI.WritePlayerEvent(new WriteClientPlayerEventRequest
            {
                Body = new Dictionary<string, object>()
            {
                {"Pokemon Name",m_pokemonName},
                {"Latitude",p_latitude},
                {"Longitude",p_longitude}
            },
                EventName = "player_caught_pokemon"
            },
            result =>
            {
                Debug.Log("Successfully uploaded event " + result.EventId);
                m_pokemonCount++;
                if (m_pokemonController.m_pokemonErrorText.enabled == false)
                {
                    m_pokemonController.m_pokemonErrorText.enabled = true;
                }
                m_pokemonController.m_pokemonErrorText.text = "Gotcha!";
                m_leaderboard.OnPostLeaderboardData(m_pokemonCount);

            },
            error =>
            {
                Debug.Log("Failed in uploading event");
            });
    }

}

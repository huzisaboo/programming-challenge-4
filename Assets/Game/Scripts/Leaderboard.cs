﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaderboard : MonoBehaviour
{
    public string m_totalPokemonCaughtString = "Total Pokemon Caught: "; 
    private int m_pokemonCount;
    public void OnPostLeaderboardData(int p_pokemonCount)
    {
        m_pokemonCount = p_pokemonCount;
        PlayFabClientAPI.UpdatePlayerStatistics(
            new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate>
                {
                   new StatisticUpdate { StatisticName = "Number_Of_Pokemon_Caught",Value = p_pokemonCount }
                }
            },
            new System.Action<UpdatePlayerStatisticsResult>(UpdatePlayerStatisticsResponse),
            new System.Action<PlayFabError>(LeaderboardErrorCallback)
            );
    }

    public void OnGetLeaderboardScore()
    {
        PlayFabClientAPI.GetPlayerStatistics(

            new GetPlayerStatisticsRequest
            {
                StatisticNames = new List<string>
                {
                    "Number_Of_Pokemon_Caught"
                }
            },
            new System.Action<GetPlayerStatisticsResult>(PlayerStatisticsResultCallback),
            new System.Action<PlayFabError>(LeaderboardErrorCallback)
            );
    }

    private void PlayerStatisticsResultCallback(GetPlayerStatisticsResult p_result) //Data fetched success callback
    {
        if(p_result.Statistics.Count > 0)
        {
            foreach (var a_statResult in p_result.Statistics)
            {
                if (a_statResult.StatisticName == "Number_Of_Pokemon_Caught")
                {
                    m_pokemonCount = a_statResult.Value;
                    UIManager.Instance.m_totalPokemonCaughtText.text = m_totalPokemonCaughtString + a_statResult.Value.ToString();
                    break;
                }
            }
        }
        else
        {
            OnPostLeaderboardData(0);
        }
    }

    private void UpdatePlayerStatisticsResponse(UpdatePlayerStatisticsResult p_result)  //Data post success callback
    {
        Debug.Log("User statistics updated");
        
            UIManager.Instance.m_totalPokemonCaughtText.text = m_totalPokemonCaughtString + m_pokemonCount.ToString();
    }

    private void LeaderboardErrorCallback(PlayFabError p_error) //Error callback same for both cases
    {
        Debug.LogError("Leaderboard had an error:");
        Debug.LogError(p_error.GenerateErrorReport());
    }

    public int GetPokemonCount()
    {
        return m_pokemonCount;
    }
}

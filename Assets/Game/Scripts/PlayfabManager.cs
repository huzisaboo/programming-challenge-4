﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayfabManager : Singleton<PlayfabManager>
{
    public enum LoginState
    {
        Startup,
        Instantiated,
        Success,
        Failed
    }

    public LoginState m_loginState = LoginState.Startup;

    public Leaderboard m_leaderboard;

    public void Login(string p_playerID)
    {
        m_loginState = LoginState.Instantiated;

        LoginWithAndroidDeviceIDRequest a_request = new LoginWithAndroidDeviceIDRequest { AndroidDeviceId = p_playerID, CreateAccount = true };
        PlayFabClientAPI.LoginWithAndroidDeviceID(a_request, OnLoginSuccess, OnLoginFailure);

    }



    private void OnLoginSuccess(LoginResult p_result)
    {
        m_loginState = LoginState.Success;

        Debug.Log("Congratulations you have logged in to playfab");
        UIManager.Instance.m_loginPanel.SetActive(false);
        GetAccountInfoRequest accountInfoRequest = new GetAccountInfoRequest { PlayFabId = p_result.PlayFabId };
        PlayFabClientAPI.GetAccountInfo(accountInfoRequest, OnAccountInfoRetrieved, OnAccountInfoRetrievedFailed);
    }

    private void OnLoginFailure(PlayFabError p_error)
    {
        m_loginState = LoginState.Failed;
        Debug.LogWarning("Something went wrong while logging into playfab");
        Debug.LogError(p_error.GenerateErrorReport());
    }

    private void OnAccountInfoRetrieved(GetAccountInfoResult p_result)
    {
        UIManager.Instance.SetWelcomeString(p_result.AccountInfo.TitleInfo.DisplayName);    //Retrieving username from Playfab (Will be null in case when account is created)
                                                                                            //But that's handled on the other end   
        m_leaderboard.OnGetLeaderboardScore();  //Retrieving stored leaderboard score
    }


    private void OnAccountInfoRetrievedFailed(PlayFabError p_error)
    {
        Debug.LogWarning("Something went wrong while fetching user info playfab");
        Debug.LogError(p_error.GenerateErrorReport());
    }
}

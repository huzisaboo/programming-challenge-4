﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Updates or sets the username of a player
public class PlayerController : MonoBehaviour
{
    private string m_playerID;
    // Start is called before the first frame update
    void Start()
    {
        m_playerID = SystemInfo.deviceUniqueIdentifier;
    }


    public void OnLoginButtonClicked()
    {
        UIManager.Instance.m_loggingInText.enabled = true;
        PlayfabManager.Instance.Login(m_playerID);                  //Logging in through unique device identifier
        LocationServiceController.Instance.InitializeLocation();   
    }

    public void OnUpdateUsernameButtonClicked()
    {
        string a_username = UIManager.Instance.GetUsername();
        if(!string.IsNullOrEmpty(a_username))
        {
            PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest { DisplayName = a_username },
           result =>
           {
               Debug.Log("Username updated successfully");
               UIManager.Instance.SetUsernametoEmpty();
               UIManager.Instance.SetWelcomeString(a_username);
           },
           error =>
           {
               Debug.Log("Failed to update username");
           }
           );
        }
    }

}
